package com.amos;

public class InvalidPDException extends Exception {

    public InvalidPDException(String s){
        super(s);
    }
}
