package com.amos;


import com.amos.logger.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

interface Iterator<T extends Object>{

    void itearotor(T i);
}


public class Util {

    static Logger logger=Logger.getInstance(Util.class);
    public static byte[] stringToByteArray(String txt){
        txt=txt.replaceAll("\r","").replaceAll("\n","");
        String[] strarr=txt.split("\\,");//txt.contains("\\,") ? txt.split("\\,") : (txt.contains("\\.") ? txt.split("\\.") : txt.split(" "));
        byte [] result= new byte[strarr.length];

        for(int i1=0;i1<result.length;i1++){

            result [i1]=(byte)Integer.parseInt(strarr[i1].trim(),16);
        }
        //forach(strarr,(o)->Byte.parseByte(((String) o).trim(),16));
        return result ;
    }



    public static void forach(Object array,Iterator iterator){
        if(array instanceof byte[]){
            byte[] temp=(byte[])array;
            for(int i=0;i<temp.length;i++){
                iterator.itearotor(temp [i]);
            }

        }else  if(array instanceof String[]){
            String[] temp=(String[])array;
            for(int i=0;i<temp.length;i++){
                iterator.itearotor(temp [i]);
            }

        }
        else  if(array instanceof int[]){
            int[] temp=(int[])array;
            for(int i=0;i<temp.length;i++){
                iterator.itearotor(temp [i]);
            }

        }

    }

    public static byte[] subByteArray(int st,int offset,byte[] array){

        assertError(st+offset<array.length,"st+offset<array.length");
        byte[] newArray= new byte[offset];
        System.arraycopy(array,st,newArray,0,offset);


        return newArray;
    }

    public static void assertError(boolean b, String s) {

        if(!b){
            logger.error("operator assert: "+s);
            throw new RuntimeException("operator assert: "+s);
        }

    }

    public static String readFileAsString(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }


    public static int[] reversArray(int ar[]){
        for (int i=0,end=ar.length-1;i<end;i++,end--){
            int temp=ar[i];
            ar[i]=ar[end];
            ar[end]=temp;
        }
        return ar;
    }

    public static int[] getBits(byte b) {
        if(b==0)
            return new int[]{0,0,0,0,0,0,0,0};

        int[] res = new int[8];
        for (int i = 0; i <8; i++) {
            res[i] = (b >> i) & 1;
        }

        return res;
    }


    public static  String arrayToHexString(int[] array){
        assertError(array!=null,"array!=null");
        StringBuilder ss = new StringBuilder();
        forach(array,(e)->ss.append(String.format("%02X ", e)));
        return ss.toString();
    }
}
