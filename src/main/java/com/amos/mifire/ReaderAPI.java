package com.amos.mifire;

import CardReader.CardReader;
import com.amos.logger.Logger;
import signalman.Signalman;

public class ReaderAPI {

    private CardReader reader = new CardReader(2);

    static Logger logger=Logger.getInstance(ReaderAPI.class);

    public  int[] uidArray;

    public String uid;
    public void initUid(String uid){
        this.uid=uid;
    }

   // public CardData getCardData() {
   //     if(block !=null || block.length!=0)
   //         return CardDataFactory.getCardData(block);
   //     else return null;
   // }



    public ReaderAPI() {

        try {
            logger.debug("return connect value: " +reader.connect());
            reader.mfrcOn();
            logger.info("connect to reader");
        }catch (Exception e){
            logger.error("ERROR CONNECT",e);
        }
    }



    public boolean isHaveUID() throws Signalman.CardReaderSignalmanException, Signalman.CardReaderSignalmanFatalException {
        uidArray = reader.activateIdleA();
        if (uidArray != null && uidArray.length!=0){
            return true;
        }
        return false;
    }

    public int[] getBlock(int blockNumber) throws Signalman.CardReaderSignalmanFatalException, Signalman.CardReaderSignalmanException, CardReader.CardReaderException {
        return reader.mifareStandartRead(blockNumber);
    }

    public void putKey(int[] uid,int[] key,int blockNumber) throws Signalman.CardReaderSignalmanFatalException, Signalman.CardReaderSignalmanException, CardReader.CardReaderException {

        reader.mifareStandartAuthKeyDirect(uid, key, blockNumber);
    }

    public int[] getUID() throws Signalman.CardReaderSignalmanException, Signalman.CardReaderSignalmanFatalException {
        return reader.activateIdleA();
    }
    /**
     * Скидка доступна.
     */
   // public boolean isAvailable() throws Exception
   // {
   //     block = new int[0];
//
   //     int[] uid = reader.activateIdleA();
   //     uidArray=uid;
   //     if (uidArray != null && uidArray.length!=0)
   //     {
   //         int blockNumber = getAbsolutBlockNumber(SettingsAmppapi.BLOCK, SettingsAmppapi.SEKTOR);
   //         logger.info("UID=" + arrayToHexString(uid));
   //         reader.mifareStandartAuthKeyDirect(uid, SettingsAmppapi.WORCK_KEY, blockNumber);
//
   //         this.block = reader.mifareStandartRead(blockNumber);
//
   //         logger.info("block on card=" + arrayToHexString(block));
   //         CardData cardata = CardDataFactory.getCardData(block);
   //         logger.info(cardata.toString());
   //         boolean result= (cardata.getPayStatus()==CardData.PayStatus.CONSESSION_WROTE);
   //         if (result) {
   //             logger.info("discount is aviable");
   //             cardata.setPayStatus(CardData.PayStatus.CARD_PAYED);
   //             int[] writeBlock = CardDataFactory.cardDataToByteCode(cardata, false);
   //             reader.mifareStandartWrite(blockNumber, writeBlock);
   //             logger.info("discount turn to CARD_PAYED");
   //         }
//
   //         return result;
   //     }
   //     return false;
   // }





    public  int getAbsolutBlockNumber(int block,int sektor){
        return sektor * 4 + block;
    }

    private String arrayToHexString(int []array) {

        StringBuilder line = new StringBuilder();
        for (int i : array) {
            line.append(" " + Integer.toHexString(i));
        }
        return line.toString();
    }
}
