package com.amos;

import com.amos.logger.Config;
import com.amos.logger.Level;
import com.amos.logger.Logger;
import com.amos.mifire.ReaderAPI;
import com.amos.trainTickets.PdSixteen;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;

import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.pdf417.PDF417Reader;
import signalman.Signalman;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;


/**
 4.40	Тарифы (для ПД без места)
 Таблица 79 – Тарифы (для ПД без места)
 № п/п	Наименование поля 	Тип 	Обязательность	Ограничение	Уникальность	Примечание
 1. 	Идентификатор тарифа 	Целый 	Да		да
 2. 	Код тарифного плана	целый	Да	Ссылка на таблицу «Тарифные планы»
 3. 	Номер маршрута 	Целый
 5 зн. 	Нет	Ссылка на запись в таблице «Маршруты»
 4. 	Код станции отправления 	Целый
 7 зн. 	Да	Ссылка на запись в таблице «Станции»
 5. 	Код станции назначения 	Целый
 7 зн. 	Да	Ссылка на запись в таблице «Станции»
 6. 	Вид ПД ЦППК	целый	Нет	Ссылка на таблицу «Виды ПД ЦППК»
 7. 	Стоимость билета, руб	денежный	Да	до 7 цифр	нет	В рублях с точностью до копеек
 8. 	Тарифный пояс отправления (специальный)	целый	Нет	Ссылка на запись в таблице «Тарифные пояса»		Используется на турникетах для тарифов на специальные предложения
 Расширяет зону действия ПД всеми станциями, входящими в указанный тарифный пояс
 9. 	Тарифный пояс назначения (специальный)	целый	Нет	Ссылка на запись в таблице «Тарифные пояса»		Используется на турникетах для тарифов на специальные предложения
 Расширяет зону действия ПД всеми станциями, входящими в указанный тарифный пояс

 */
public class App 
{

    static {
        Config config=Config.getConfig();
        config.setFileName("logs/test.log");
        config.setLevel(Level.FILE);

    }
   static Logger logger=Logger.getInstance(App.class);
    static final int[] KEY_ZERO_SECTOR={0xA0,0xA1,0xA2,0xA3,0xA4,0xA5};
    static final int[] KEY_TWO_SECTOR_ONE={0x2A,0xBA,0x95,0x19,0xF5,0x74};
    static final int[] DEFOULT_KEY =  {0xFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF};
    static final int[] SIX_KEY =  {0x2A, 0XA0, 0X5E, 0XD1, 0X85, 0X6F};

    public static void main( String[] args ) throws IOException, FormatException, ChecksumException, NotFoundException, InvalidPDException, Signalman.CardReaderSignalmanException, Signalman.CardReaderSignalmanFatalException {

try {
    logger.info("transport card:");

    ReaderAPI readerAPI = new ReaderAPI();

    int[] uid = readerAPI.getUID();

    logger.info("uid:" + Util.arrayToHexString(uid));

   readerAPI.putKey(uid, KEY_ZERO_SECTOR, 0);

    int[] data = readerAPI.getBlock(0);
    logger.info("data:" + Util.arrayToHexString(data));

    logger.info("_________________________________________");
}catch (Exception e){

    logger.error(e.getMessage(),e);
}
if(1>0) return;;
               PDF417Reader reader= new PDF417Reader();

               File srcFile = new File("image/018091.jpg");
               BufferedImage image = ImageIO.read(srcFile);

               LuminanceSource source = new BufferedImageLuminanceSource(image);

               BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

               Result result=reader.decode(bitmap);
               logger.info(new String( result.getText().getBytes(StandardCharsets.ISO_8859_1)));


               byte[] res=result.getText().getBytes(StandardCharsets.ISO_8859_1);
        logger.info("длинна сообщения: "+res.length);
               Util.forach(res,(value)->System.out.print(String.format("%x, ",value)));
               System.out.println();


               PdSixteen parserTicket = (PdSixteen) ParserTicket.getTicket(res);
        logger.info("номер билета:"  +parserTicket.getNumberTicket());
        logger.info("дата покупки:"  +parserTicket.getDate());

        logger.info(parserTicket.getNumberPhon());


               String round=!parserTicket.isRoundWayTicket() ? "в одну сторону" : "туда и обратно";
               String nal=parserTicket.isCashe() ? "cash" : "electronic";
               String checkOpen=parserTicket.isCheckOpen() ? "не проверять на входе" : "проверять на входе";
               String activTicket=parserTicket.isNeedActive() ? "не надо активировать" : "активировать";

        logger.info("флаги:---------------------------");
        logger.info("билет "+round);
        logger.info("проверка на входе: "+checkOpen);
        logger.info("расчет: "+nal);
        logger.info("активация: "+activTicket);
        logger.info("---------------------------------");


               String sroki = parserTicket.getTern()==0 ? "в день покупки" : " в течении "+parserTicket.getTern() +" с момента покупки";
        logger.info("действителен "+sroki);


        logger.info("код льготы: "+parserTicket.getCodeLgote());


        logger.info("Тариф: "+parserTicket.getTariff());






    }


}
