package com.amos;

import com.amos.trainTickets.PdSixteen;
import com.amos.trainTickets.PdTwo;
import com.google.gson.Gson;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

public abstract class ParserTicket {



    private byte pd;


    private int    numberTicket;






    protected byte[] data;
    protected int tarrif;
    protected Date date;
    private boolean isCashe;
    private boolean roundWayTicket;
    private boolean isNeedActive;
    private boolean isCheckOpen;

    protected final ByteOrder LITTLE_ENDIAN = ByteOrder.LITTLE_ENDIAN;

//valid PD
    /*
    * 1, Разовый ПД без места	Полный или льготный, в виде штрих-кода
    * 2, Доплата за класс поезда	Полный или льготный, в виде штрих-кода
    * 9, Разовый ПД с местом на ШК	Полный или льготный, в виде штрих-кода
    * 22,Разовый ПД в виде штрих-кода для Агентов
    * */

    public  ParserTicket(byte[] data) {
           this.data=data;
          if(data==null)
              throw new NullPointerException("array is null");

          pd =data[0];

          parseTicketNumber(data);
          int [] bits= Util.getBits(data[3]);
          roundWayTicket=bits[0]!=0;
          isCashe=bits[1]==0;
          isCheckOpen=bits[2]==0;
          isNeedActive=bits[3]==0;
    }

    public static ParserTicket getTicket(byte[]data) throws InvalidPDException {
        if(data[0] ==0x16){
            return new PdSixteen(data);
        }else if (data[0] ==0x1) {
            return new PdTwo(data);
        }else if(data[0] ==0x2){
          //  dateArray = Util.subArray(11,4, data);
            return null;
        }else if(data[0] ==0x9){
            //???  dateArray = Util.subArray(11,4, data);
            return null;
        }else{
            throw new InvalidPDException("not found pd - "+ data[0]);
        }

    }

    //На КО это порядковый номер (не фискальный) чека за календарный месяц.
    // Битовая упаковка, конвертируется в 4-байтное little-endian число:
    // 0 и 1 байт Упаковки копируются 0 и 1 байт Числа,
    // 6 и 7 биты 3 байта - в
    // 0 и 1 биты 2-го байта Числа.
    private void parseTicketNumber(byte[] data){
        byte [] numberTicketBytes = new byte[4];
        numberTicketBytes[0]=data[1];
        numberTicketBytes[1]=data[2];
        int [] three=Util.getBits(data[3]);
        byte twoByte  = Byte.parseByte("000000"+three[6]+""+three[7],2);
        numberTicketBytes[2]=twoByte;
        ByteBuffer numberTicketBuffer=ByteBuffer.wrap(numberTicketBytes).order(LITTLE_ENDIAN);
        numberTicket = numberTicketBuffer.getInt();
    }


    public int getPd() {
        return pd;
    }

    public abstract Date getDate();


    public abstract int getTariff();

    public int getNumberTicket() {
        return numberTicket;
    }


    //public short getCodeLgote() {   return codeLgote; }

    public abstract byte getTern();
    public abstract short getCodeLgote();
    public abstract String getNumberPhon();

    public int getRate() {
        return 0;
    }

    public byte getDeadlines() {
        return 0;
    }

    public boolean isCashe() {
        return isCashe;
    }

    public boolean isRoundWayTicket() {
        return roundWayTicket;
    }

    public boolean isNeedActive() {
        return isNeedActive;
    }

    public boolean isCheckOpen() {
        return isCheckOpen;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
