package com.amos.trainTickets;


import com.amos.ParserTicket;
import com.amos.Util;

import java.nio.ByteBuffer;
import java.util.Date;

public class PdSixteen extends ParserTicket {

    public PdSixteen(byte[] data) {
        super(data);

        byte[] tarifB = Util.subByteArray(16, 4, data);
        ByteBuffer buffetTarif = ByteBuffer.wrap(tarifB).order(LITTLE_ENDIAN);
        int res = buffetTarif.getInt();
        tarrif=res;

            byte[] dateArray = Util.subByteArray(12, 4, data);
            ByteBuffer dateArrayBuffer = ByteBuffer.wrap(dateArray).order(LITTLE_ENDIAN);
             date= new Date(dateArrayBuffer.getInt() * 1000L);



    }

    @Override
    public Date getDate() {
return date;
    }

    @Override
    public int getTariff() {

        return tarrif;
    }

    @Override
    public byte getTern() {
        return data[9];
    }

    @Override
    public short getCodeLgote() {
        byte[] codeLg=Util.subByteArray(10,2,data);
        ByteBuffer lgote=ByteBuffer.wrap(codeLg).order(LITTLE_ENDIAN);
        return lgote.getShort();
    }

    @Override
    public String getNumberPhon() {
        ByteBuffer phon = ByteBuffer.wrap(Util.subByteArray(4, 5, data)).order(LITTLE_ENDIAN);
        return "Номер телефона: "+new String(phon.array()).replaceAll("\\s","?").replaceAll("","?");
    }


}
