package com.amos.trainTickets;


import com.amos.ParserTicket;
import com.amos.Util;

import java.nio.ByteBuffer;
import java.util.Date;

public class PdTwo extends ParserTicket {

    public PdTwo(byte[] data) {
        super(data);
    }

    @Override
    public Date getDate() {
        byte[]  dateArray = Util.subByteArray(4,4, data);
        ByteBuffer dateArrayBuffer=ByteBuffer.wrap(dateArray).order(LITTLE_ENDIAN);
        return new Date(dateArrayBuffer.getInt()*1000L);
    }

    @Override
    public int getTariff() {
        return 0;
    }

    @Override
    public byte getTern() {
        return 0;
    }

    @Override
    public short getCodeLgote() {
        return 0;
    }

    @Override
    public String getNumberPhon() {
        return null;
    }
}
